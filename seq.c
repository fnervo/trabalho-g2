#include "seq.h"
#include <stdlib.h>
#include <string.h>

void printItemsSum(int *sequence) {
    int size = sizeof(sequence),
        sum = 0;
    
    for (int i = 0; i < size; i++) {
        sum += sequence[i];
    }

    printf("A soma dos itens é igual a %d\n", sum);
}

FILE *readFile(char *filename) {
    FILE *fp = fopen(filename, "r");
    return fp;
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        printf("Erro: é necessário informar o nome do arquivo");
        return 1;
    }

    char *filename = argv[1];
    FILE *fp = readFile(filename);
    if (fp == NULL) {
        printf("Erro: ocorreu um erro ao ler o arquivo");
        return 1;
    }

    // reading line by line, max 256 bytes
    const unsigned MAX_LENGTH = 256;
    char buffer[MAX_LENGTH];

    int item,
        sequenceQuantity = -1,
        itemsRemaining = 0,
        index = 0;

    int *sequence;

    while (fgets(buffer, MAX_LENGTH, fp)) {
        item = atoi(buffer);

        if (sequenceQuantity == -1) {
            sequenceQuantity = item;
            printf("Serão processadas %d sequências...\n", 
                sequenceQuantity);
        } else if (itemsRemaining == 0) {
            printf("Iniciando o processamento de uma sequência...\n");

            itemsRemaining = item;

            sequence = malloc(itemsRemaining);
            
            index = 0;

        }  else {

            sequence[index] = item;
            itemsRemaining--;

            if (itemsRemaining == 0) {

                printItemsSum(sequence);
                
                printf("Desalocando a sequência da memória...\n");

                free(sequence);
            } else {
                index++;
            }
        }
    }

    fclose(fp);

    return 0;
}
